pragma solidity ^0.8.0;

import "hardhat/console.sol";

contract firstContract {
    uint totalWaves;

    constructor() {
        console.log("Hello, Web3!");
    }

    function wave() public {
        totalWaves += 1;
        console.log('Just got a wave!');
    }

    function getTotalWaves() public view returns (uint) {
        return totalWaves;
    }
}