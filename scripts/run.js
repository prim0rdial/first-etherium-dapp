const main = async() => {
    // compile my contracts
    const contractFactory = await hre.ethers.getContractFactory("firstContract");

    // deploy it
    const contract = await contractFactory.deploy();

    // let me know when its done deploying
    // have to know when we are done mining
    await contract.deployed(); // wait for the contract to be deployed

    console.log("Deployed 🎉 ! Address: ", contract.address);

    let txn = await contract.wave();
    await txn.wait();

    txn = await contract.wave();
    await txn.wait();
}


const runMain = async () => {
    try {
        await main();
        process.exit(0);
    } catch (error) {
        console.log(error);
        process.exit(1);
    }
}

runMain();